#!/bin/bash

# One-liner execution: 
# wget --no-check-certificate -qO- https://bitbucket.org/bobspongebob/dump/raw/master/vps_setup64-v2.sh | bash -
# No check added for broken minimal installations that can't verify certs - re-installing wget fixes this

## See: http://askubuntu.com/a/517618
apt-get update  < "/dev/null"
apt-get -y upgrade < "/dev/null"
apt-get -y install curl wget screen unzip htop git < "/dev/null"

# SSH Keys
mkdir /root/.ssh
wget https://bitbucket.org/bobspongebob/dump/raw/master/id_rsa.pub -O /root/.ssh/authorized_keys

# Tcpping (useful for azure vm without pip)
apt-get -y install tcptraceroute < "/dev/null"
wget http://www.vdberg.org/~richard/tcpping
chmod +x tcpping
mv tcpping /usr/local/bin/tcpping

# Consul
mkdir /opt/consul
wget https://dl.bintray.com/mitchellh/consul/0.4.1_linux_amd64.zip -O /opt/consul/consul.zip
(cd /opt/consul && unzip consul.zip)
mv /opt/consul/consul /usr/local/bin/consul
chmod +x /usr/local/bin/consul
rm -rf /opt/consul
mkdir -p /etc/consul.d/{bootstrap,server,client}
mkdir /var/consul
wget https://bitbucket.org/bobspongebob/dump/raw/master/csl_client_config.json -O /etc/consul.d/client/config.json # Edit manually
wget https://bitbucket.org/bobspongebob/dump/raw/master/csl_upstart_client -O /etc/init/consul.conf # Edit manualy

# Consul-template
wget --no-check-certificate https://github.com/hashicorp/consul-template/releases/download/v0.5.1/consul-template_0.5.1_linux_amd64.tar.gz
tar xzf consul-template_0.5.1_linux_amd64.tar.gz
mv consul-template_0.5.1_linux_amd64/consul-template /usr/local/bin/consul-template
rm -rf consul-template_0.5.1_linux_amd64 consul-template_0.5.1_linux_amd64.tar.gz
chmod +x /usr/local/bin/consul-template

# Tinyproxy
mkdir -p /opt/templates
wget https://bitbucket.org/bobspongebob/dump/raw/master/consul_tinyproxy_template -O /opt/templates/tinyproxy.conf
wget https://bitbucket.org/bobspongebob/dump/raw/master/csl_upstart_consul_tinyproxy -O /etc/init/consul-tinyproxy.conf
wget https://bitbucket.org/bobspongebob/dump/raw/master/conul_proxy_config -O /etc/consul.d/client/proxy1.json # Edit manually

# Node.js
wget -qO- https://deb.nodesource.com/setup | bash -
apt-get -y install nodejs
