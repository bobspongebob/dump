#!/bin/bash

# One-liner execution: 
# wget -qO- https://bitbucket.org/bobspongebob/dump/raw/master/vps_setup.sh | bash -

## See: http://askubuntu.com/a/517618
apt-get update  < "/dev/null"
apt-get -y upgrade < "/dev/null"
apt-get -y install curl wget screen unzip htop git mercurial make binutils bison gcc build-essential < "/dev/null"

# SSH Keys
mkdir /root/.ssh
wget https://bitbucket.org/bobspongebob/dump/raw/master/id_rsa.pub -O /root/.ssh/authorized_keys

# GVM
bash < <(curl -s -S -L https://raw.githubusercontent.com/moovweb/gvm/master/binscripts/gvm-installer)
## only works downstream, do it manually to have it available on the existing shell
source /root/.gvm/scripts/gvm 
gvm install go1.4 -B
gvm use go1.4 --default

# Consul
mkdir /opt/consul
wget https://dl.bintray.com/mitchellh/consul/0.4.1_linux_386.zip -O /opt/consul/consul.zip
(cd /opt/consul && unzip consul.zip)
mv /opt/consul/consul /usr/local/bin/consul
chmod +x /usr/local/bin/consul
rm -rf /opt/consul
mkdir -p /etc/consul.d/{bootstrap,server,client}
mkdir /var/consul
wget https://bitbucket.org/bobspongebob/dump/raw/master/csl_client_config.json -O /etc/consul.d/client/config.json # Edit manually
wget https://bitbucket.org/bobspongebob/dump/raw/master/csl_upstart_client -O /etc/init/consul.conf

# Consul-template
pushd /opt
git clone https://github.com/hashicorp/consul-template.git
(cd consul-template && make)
mv /opt/consul-template/bin/consul-template /usr/local/bin/consul-template
chmod +x /usr/local/bin/consul-template
popd

# Node.js
wget -qO- https://deb.nodesource.com/setup | bash -
apt-get -y install nodejs
